import csv
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from tkinter import *
import matplotlib.pyplot as plt
import ctypes

InputData = []
Output = []
scaler = StandardScaler()
reg = MLPRegressor(hidden_layer_sizes=(600,), activation='relu', solver='lbfgs',
                   alpha=0.001, batch_size='auto',
                   learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True,
                   random_state=0, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,
                   early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)


def Datapreparation():
    with open('Concrete_Data.csv', newline='')as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            temp = [float(i) for i in row]
            Output.append(temp[len(temp) - 1])
            temp.pop()
            InputData.append(temp)

    scaler.fit(InputData)


def Training(training_input, training_output):
    training_input = scaler.transform(training_input)
    reg.fit(training_input, training_output)


def Get_prediction(test_input):
    test_input = scaler.transform(test_input)
    pred = reg.predict(test_input)
    return pred


def system_performance():
    InputData_train, InputData_test, Output_train, Output_test = train_test_split(InputData, Output)
    Training(InputData_train, Output_train)
    prediction = Get_prediction(InputData_test)

    error = [abs(Output_test[i] - prediction[i]) for i in range(len(prediction))]
    print("average error :", sum(error) / len(error))
    print("max error : ", max(error))
    print("min error : ", min(error))

    plt.plot(Output_test, 'r')
    plt.plot(prediction, 'b')
    plt.show()


Datapreparation()
#system_performance()
Training(InputData,Output)

root = Tk()
frame = Frame(root, width=515, height=400)
frame.pack()
label1 = Label(frame,bg='orange', text='Cement (component 1)(kg in a m^3 mixture)')
label1.place(x=20, y=20)
label2 = Label(frame,bg='yellow', text='Blast Furnace Slag (component 2)(kg in a m^3 mixture)')
label2.place(x=20, y=45)
label3 = Label(frame, bg='orange',text='Fly Ash (component 3)(kg in a m^3 mixture)')
label3.place(x=20, y=70)
label4 = Label(frame,bg='yellow', text='Water  (component 4)(kg in a m^3 mixture)')
label4.place(x=20, y=95)
label5 = Label(frame,bg='orange', text='Superplasticizer (component 5)(kg in a m^3 mixture)')
label5.place(x=20, y=120)
label6 = Label(frame,bg='yellow', text='Coarse Aggregate  (component 6)(kg in a m^3 mixture)')
label6.place(x=20, y=145)
label7 = Label(frame,bg='orange', text='Fine Aggregate  (component 6)(kg in a m^3 mixture)')
label7.place(x=20, y=170)
label8 = Label(frame,bg='yellow', text='Age (day)')
label8.place(x=20, y=195)

E1 = Entry(frame,bg='orange', width=20)
E1.place(x=330, y=20)
E2 = Entry(frame,bg='yellow', width=20)
E2.place(x=330, y=45)
E3 = Entry(frame,bg='orange', width=20)
E3.place(x=330, y=70)
E4 = Entry(frame,bg='yellow', width=20)
E4.place(x=330, y=95)
E5 = Entry(frame,bg='orange', width=20)
E5.place(x=330, y=120)
E6 = Entry(frame,bg='yellow', width=20)
E6.place(x=330, y=145)
E7 = Entry(frame,bg='orange', width=20)
E7.place(x=330, y=170)
E8 = Entry(frame,bg='yellow', width=20)
E8.place(x=330, y=195)

def Getresult():
    test = []
    test.append(float(E1.get()))
    test.append(float(E2.get()))
    test.append(float(E3.get()))
    test.append(float(E4.get()))
    test.append(float(E5.get()))
    test.append(float(E6.get()))
    test.append(float(E7.get()))
    test.append(float(E8.get()))

    ans = Get_prediction([test])
    ctypes.windll.user32.MessageBoxW(0, str(ans) , "Concrete compressive strength(MPa, megapascals)", 0)

button1 = Button(frame,command = Getresult ,bg='light blue',text='Concrete compressive strength(MPa, megapascals)', width=50, height=2 ,activebackground = 'light green')
button1.place(x=80,y=280)

root.mainloop()
